#include <iostream>
#include <cmath>
#include "cuadratura.h"

using namespace std;

double g(double);

int main(int argc, char *argv[]){
  int n = atoi(argv[1]);
  cout << Cuadratura(g,0,M_PI).Romberg(n) << endl;
  cout << Cuadratura(g,0,M_PI).Gauss(n) << endl;
  return 0;
}

double g(double x){
  return 2*sin(x)*sin(x);
}

