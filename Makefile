src = vec_operators.h matriz.h
obj = cuadratura.o

main: main.cc $(src) $(obj)
	g++ $@.cc $(obj) -o $@.exe

$(obj): %.o:%.cc %.h
	g++ -c $< -o $@

gauss: $(src) gauss.cc
	g++ $@.cc -o $@.exe

clean:
	@rm -f *~ *.exe *.o
