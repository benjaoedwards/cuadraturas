#ifndef CUADRATURA_H
#define CUADRATURA_H

#include <cmath>
#include "matriz.h"

using namespace std;

class Cuadratura{
  double (*f)(double);
  double a;
  double b;
  int n;
  void integrate();
public:
  Cuadratura(double (*)(double),double, double);
  double Romberg(int);
  double Gauss(int);

};
#endif
