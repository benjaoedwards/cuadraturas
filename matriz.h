#ifndef MATRIZ_H
#define MATRIZ_H
#include <iostream>
#include <vector>
#include <cmath>
#include "vec_operators.h"

using namespace std;

template <class T>
class Matriz{
private:
  vector<T> aij;
  int f,c,dim;
  
public:
  Matriz();
  Matriz(int,int);
  Matriz(int,int,vector<T>);
  ~Matriz();
  //Matriz(const Matriz &); //
  void set_aij(vector<T>);
  int get_dim();
  int get_f();
  int get_c();
  vector<T> get_aij();
  vector<T> fila(int);
  //Matriz & operator= (const Matriz<T> &); //
  T & operator()(int,int); //
  T operator()(int,int) const; //
  T tr();
  Matriz<T> swap_f(int, int);
  Matriz<T> set_fila(int,vector<T>);
  Matriz<T> escalonar();
  Matriz<T> reduccion_gauss();
  Matriz<T> montante(bool det = false);
  Matriz<T> inversa();
  T det_old();
  T det();
};

//////////////// FUNCIONES MATRIZ //////////////////////

///////////////// CONSTRUCTOR ///////////////////////////

template <class T>
Matriz<T>::Matriz(){
}

template <class T>
Matriz<T>::Matriz(int a,int b) {
  f=a;
  c=b;
  dim=f*c;
  for (int i=0;i<dim;++i)
    {
      aij.push_back(0);
    }
} 

template <class T>
Matriz<T>::Matriz(int a,int b,vector<T> v) {
  f=a;
  c=b;
  aij = v;
}

//////////////////////////////////////////////////////

template <class T>
Matriz<T>::~Matriz(){
}

template <class T>
int Matriz<T>::get_dim(){
  return dim;
}

template <class T>
int Matriz<T>::get_f(){
  return f;
}
template <class T>
int Matriz<T>::get_c(){
  return c;
}

// RETORNA VECTO DE COEFICIENTES
template <class T>
vector<T> Matriz<T>::get_aij(){
  return aij;
}

template <class T>
void Matriz<T>::set_aij(vector<T> a){
  aij = a;
}

// RETORNA ELEMENTO ij
template <class T>
T & Matriz<T>::operator()(int i,int j){ // obtener elemento aij
  return aij[(i-1)*c+j-1];
}

template <class T>
T Matriz<T>::operator()(int i,int j) const { // obtener elemento aij
  return aij[(i-1)*c+j-1];
}

// RETORNADOR FILA
template <class T>
vector<T> Matriz<T>::fila(int i){
  vector<T> fila;
  for (int j = 1;j<=c;j++){
    fila.push_back((*this)(i,j));
  }
  return fila ;
}

// COPIA
/*template <class T>
Matriz<T>::Matriz(const Matriz<T> & A){
  f=A.get_f();
  c=A.get_c();
  aij=A.get_aij();
  }*/

////////////////////////////////////////////////////////

///////////////// SOBRECARGA DE OPERADORES /////////////

// ASIGNACION
/*template <class T>
Matriz<T> & Matriz<T>::operator= (const Matriz<T> & A){
  f=A.get_f();
  c=A.get_c();
  aij=A.get_aij();
  return *this;
  }*/

// IMPRESION
template <class T>
ostream & operator << (ostream & os ,Matriz<T> M){
  int f=M.get_f();
  int c=M.get_c();
  vector<T> aij=M.get_aij();
  for (int i=1;i<=f;++i){
    for (int j=1;j<=c;++j){
      os << M(i,j) << " ";
    }
    os << endl;
  }
  return os;
}

// SUMA
template <class T>
Matriz<T> operator + (Matriz<T> A,Matriz<T> B){
  if ((A.get_f()==B.get_f())&&(A.get_c()==B.get_c())){
    vector<T> aij = A.get_aij()+B.get_aij();
    return Matriz<T>(A.get_f(), A.get_c(), aij);    
  }
  else{
    cerr << "Las matrices no tienen la misma dimensión. No se pueden sumar" << endl ;
    throw ;
  }
}

// RESTA
template <class T>
Matriz<T> operator - (Matriz<T> A,Matriz<T> B){
  if ((A.get_f()==B.get_f())&&(A.get_c()==B.get_c())){
    vector<T> aij = A.get_aij()-B.get_aij();
    return Matriz<T>(A.get_f(), A.get_c(), aij);    
  }
  else{
    cerr << "Las matrices no tienen la misma dimensión. No se pueden restar" << endl ;
    throw ;
  }
}

// MULTIPLICACION
template <class T>
Matriz<T> operator * (Matriz<T> A,Matriz<T> B){
  int f=A.get_f();
  int c=B.get_c();
  double EPS = 1e-10;
  vector<T> aij;
  if (f==c){
    for (int i=1; i<=f;i++){
      for (int j=1;j<=f;j++){
	T elemento = T(0);
	for (int k=1;k<=A.get_c();k++){
	  elemento+=A(i,k)*B(k,j);
	}
	if (elemento < EPS) elemento = T(0);
	aij.push_back(elemento);
      }
    }
  }
  else{
    cerr << "Las matrices no se pueden multiplicar" << endl;
    throw;
  }
  return Matriz<T>(f,c,aij);
}

// MULT ESCALAR
template <class T>
Matriz<T> operator * (T x, Matriz<T> M){
  vector<T> aij;
  aij = x*M.get_aij();
  return Matriz<T>(M.get_f(),M.get_c(),aij);
}

// MULT VECT
template <class T>
vector<T> operator * (Matriz<T> M, vector<T> v){
  Matriz<T> Mv(v.size(),1,v);
  return M*Mv;
}

// DIV ESCALAR
template <class T>
Matriz<T> operator / (Matriz<T> M, T x){
  vector<T> aij;
  aij = M.get_aij()/x;
  return Matriz<T>(M.get_f(),M.get_c(),aij);
}

/////////////////////////////////////////////////////


////////////////// OPERACIONES DE MATRIZ //////////////////

// SWAP ROWS
template <class T>
Matriz<T> Matriz<T>::swap_f(int f1, int f2){
  vector<T> vec;
  vector<T> fila1 = (*this).fila(f1);
  vector<T> fila2 = (*this).fila(f2);
  for (int i = 1;i<=f;++i){
    for (int j=1;j<=c;++j){
      if (i == f1){
	vec.push_back(fila2[j-1]);
      }
      else if(i == f2){
	vec.push_back(fila1[j-1]);
      }
      else{
	vec.push_back((*this)(i,j));
      }
    }
    aij = vec;
  }
  return *this;
}

template <class T>
Matriz<T> Matriz<T>::set_fila(int i, vector<T> fila){
  for (int j=1;j<=c;j++){
    (*this)(i,j) = fila[j-1];
  }
  return *this;
}

template <class T>
Matriz<T> join(Matriz<T> A, Matriz<T> B) {
  int f_A = A.get_f();
  int f_B = B.get_f();
  int c_A = A.get_c();
  int c_B = B.get_c();
  if (f_A == f_B){
    vector<T> vec;
    for (int i=1;i<=f_A;i++){
      for (int k=0;k<c_A;k++){
	vec.push_back(A.fila(i)[k]);
      }
      for (int k=0;k<c_B;k++){
	vec.push_back(B.fila(i)[k]);
      }
    }
    int c = c_A + c_B;
    return Matriz<T>(f_A,c,vec);
  }
  else{
    cerr << "Las matrices no se pueden unir" << endl;
    throw;
  }
}

template <class T>
vector<Matriz<T> > split(Matriz<T> M, int n){
  vector<Matriz<T> > vec;
  vector<T> m1;
  vector<T> m2;
  int f=M.get_f();
  int c=M.get_c();
  for (int i=1;i<=f;i++){
    for (int j=1;j<=n;j++){
      m1.push_back(M(i,j));
    }
    for (int j=n+1;j<=c;j++){
      m2.push_back(M(i,j));
    }
  }
  Matriz<T> M1(f,n,m1);
  Matriz<T> M2(f,c-n,m2);
  vec.push_back(M1);
  vec.push_back(M2);
  return vec;
}

////////////////////////////////////////////

//////////////// FUNCIONES ////////////////

template <typename T>
Matriz<T> crear_I(int n){
  vector<T> vec;
  for (int i=0;i<n;i++){
    for (int j=0;j<n;j++){
      if (i == j){
	vec.push_back(T(1));
      }
      else{
	vec.push_back(T(0));
      }
    }
  }
  return Matriz<double>(n,n,vec);
}

template <class T>
Matriz<T> Matriz<T>::escalonar(){
  for (int i=1;i<=f;i++){
    if((*this)(i,i) == T(0)){
      for (int k=i+1;k<=f;k++){
	if ((*this)(k,i) != T(0)){
	  (*this).swap_f(i,k);
	  break;	  
	}
      }
    }
  }
  return *this;
}


template <class T>
Matriz<T> Matriz<T>::reduccion_gauss(){
  vector<T> fila_i;
  vector<T> fila_k;
  // ESCALONAR
  (*this).escalonar();
  // TRIANGULAR SUP
  for (int i=1;i<=f;i++){
    if ((*this)(i,i) != T(0)){
      fila_i = (*this).fila(i);
      fila_i = fila_i/(*this)(i,i);
      (*this).set_fila(i,fila_i);
      fila_i = (*this).fila(i);
      for (int k=i+1;k<=f;k++){
	fila_k = (*this).fila(k) - (*this)(k,i)*fila_i;
	(*this).set_fila(k,fila_k);
      }
      cout << (*this) << endl;
    }
    else{
      if (i == f){
  	cerr << "La matriz no se puede solucionar" << endl;
  	throw;
      }
      (*this).swap_f(i,i+1 );
    }
  }
  cout << (*this) << endl;
  // TRIANGULAR INF
  for (int i=f;i>=1;i--){
    if ((*this)(i,i) != T(0)){
      fila_i = (*this).fila(i);
      fila_i = fila_i/(*this)(i,i);
      (*this).set_fila(i,fila_i);
      for (int k=i-1;k>=1;k--){
	fila_k = (*this).fila(k) - ((*this)(k,i))*(*this).fila(i);
	(*this).set_fila(k,fila_k);
      }
    }
    else{
      if (i == f){
    	cerr << "La matriz no se puede solucionar" << endl;
    	throw;
      }
      (*this).swap_f(i,i+1 );
    }
  }
  return *this;
}

template <class T>
Matriz<T> Matriz<T>::montante(bool det){
  // ESCALONAR
  (*this).escalonar();
  
  T p = T(1);
  for (int k=1;k<=f;k++){
    T i = T(1);
    while (i <= f){
      if (i != k){
	for (int j=c;j>=1;j--){
	  (*this)(i,j) = ((*this)(k,k)*(*this)(i,j) - (*this)(i,k)*(*this)(k,j))/p;
	}
      }
      i = i + T(1);
    }
    p = (*this)(k,k);
  }
  if (det == false) (*this) = (*this)/(*this)(f,f);
  return *this;
}
  
//////////////////////////////////////////////////////

template <class T>
vector<T> solve(Matriz<T> M, vector<T> v){
  Matriz<T> Mv(v.size(),1,v);
  Matriz<T> S = join(M,Mv);
  S.reduccion_gauss();
  int n = M.get_c();
  Matriz<T> Msol = split(S,n)[1];
  vector<T> vec = Msol.get_aij();
  return vec;
}

////////////////// ELEMENTOS ///////////////////////

// TRAZA
template <class T>
T Matriz<T>::tr(){
  T tr = T(0);
  if (f == c){
    for (int i=0;i<f;i++){
      tr += (*this)(i,i);
    }
  }
  else{
    cerr << "La matriz no es cuadrada" << endl;
    throw;
  }
  return tr;
}

template <class T> 
Matriz<T> Matriz<T>::inversa(){
  Matriz<T> I = crear_I<T>(f);
  Matriz<T> AI = join((*this),I);
  //  AI.reduccion_gauss();
  AI.montante();
  Matriz<T> inv = split(AI,f)[1];
  return inv;  
}

template <class T>
T Matriz<T>::det_old(){
  vector<T> fila_k;
  double swap_count=0;
  // escalonar() pero con swap_count
  for (int i=1;i<=f;i++){
    if((*this)(i,i) == T(0)){
      for (int k=i+1;k<=f;k++){
	if ((*this)(k,i) != T(0)){
	  (*this).swap_f(i,k);
	  if ((k-1)%2 != 0){
	    swap_count += 1;
	  }
	  break;	  
	}
      }
    }
  }
  //////////////////  TRIANG SUP - Gauss-Jordan    ///////////////////
  for (int i=1;i<=f;i++){
    for (int k=i+1;k<=f;k++){
      if ((*this)(k,i) != T(0)){
	fila_k = (*this).fila(k) - (*this)(k,i)*(*this).fila(i)/(*this)(i,i);
	(*this).set_fila(k,fila_k);
      }
    }
  }
  T d=1;
  for (int i=1;i<=f;i++){
    d = d*(*this)(i,i);
  }
  d = pow(-1,swap_count)*d;
  return d; 
}

template <class T>
T Matriz<T>::det(){
  (*this).montante(1);
  return (*this)(1,1);
}

#endif
