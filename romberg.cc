#include "romberg.h"

using namespace std;

Romberg::Romberg(double (*fun)(double), double l_inf,double l_sup){
  f = fun;
  a = l_inf;
  b = l_sup;
}

void Romberg::integrate(){
  Matriz<double> M(n,n);
  M(1,1) = 0.5*(b-a)*(f(a)+f(b));

  for (int i = 1;i<n;i++){
    double h = (b-a)/(pow(2,i));
    double s = 0;
    for (int k=1;k<=pow(2,i-1);k++){
      s = s + f( a + (2*k - 1)*h );
    }
    M(i+1,1) = 0.5*M(i,1) + h*s;
  }

  for (int i=1;i<n;i++){
    for (int j = 1;j<=i;j++){
      M(i+1,j+1) = M(i+1,j) + (M(i+1,j) - M(i,j))/(pow(4,j) - 1); 
    }
  }

  integral = M(n,n);
}
  
double Romberg::operator= (int n){
  n = n;
  integrate();
  return integral;
}


