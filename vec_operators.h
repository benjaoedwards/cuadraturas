#ifndef VEC_OPERATORS_H
#define VEC_OPERATORS_H

#include <iostream>
#include <cmath>
#include <vector>

using namespace std;

// SUMA
template <class T>
vector<T> operator + (vector<T> a, vector<T> b){
  if (a.size() == b.size()){
    vector<T> v;
    for (int i = 0; i < a.size(); ++i) {
      v.push_back(a[i]+b[i]);
    }
    return v;
  }
}

// RESTA
template <class T>
vector<T> operator - (vector<T> a, vector<T> b){
  if (a.size() == b.size()){
    vector<T> v;
    for (int i = 0; i < a.size(); ++i) {
      v.push_back(a[i]-b[i]);
    }
    return v;
  }
}

// MULTIPLICACION ESCALAR
template <class T>
vector<T> operator * (T n, vector<T> b){
  vector<T> v;
  for (int i = 0; i < b.size(); ++i) {
    v.push_back(n*b[i]);
  }
  return v;
}

// DIVISION ESCALAR
template <class T>
vector<T> operator / (vector<T> b, T n){
  vector<T> v;
  for (int i = 0; i < b.size(); ++i) {
    v.push_back(b[i]/n);
  }
  return v;
}

// IMPRESION
template <class T>
ostream & operator << (ostream & os, vector<T> v){
  os << "(";
  for (int i = 0 ; i<v.size()-1 ; i++){
    os << v[i] << ",";
  }
  os << v[v.size()-1] << ")";
  return os;
}

#endif
