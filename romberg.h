#ifndef CUADRATURA_H
#define CUADRATURA_H

#include <cmath>
#include "matriz.h"

using namespace std;

class Romberg{
  double a;
  double b;
  int n;
  double integral;
  double f(double);
  void integrate();
public:
  Romberg(double (*)(double),double, double);
  double operator=(int);

};
#endif
